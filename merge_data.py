import os
import os.path
import pandas as pd
import argparse
import numpy as np

'''
to do:  maybe check subdirectories for names of columns
'''

def parser():
    parser = argparse.ArgumentParser(description='Analysis of iPS cells')
    parser.add_argument('--dir', type=str, default="", help='path to directory or single file')
    parser.add_argument('--outdir', type=str, default="", help='path to outdir')
    parser.add_argument('--head_subdirs', type=str, default="", help='header of subdirs separated by a comma (e.g. sample,cell,file)')
    parser.add_argument('--split_name', type=str, default="_", help='character that splits the filename')
    return parser.parse_args()

def main(args):
    # find files
    files = []
    # find all files ending with specified file ending
    for dirpath, dirnames, filenames in os.walk(args.dir):
        for filename in [f for f in filenames if f.endswith(".csv")]:
            filepath = os.path.join(dirpath, filename)
            if "\\" in filepath:
                filepath = filepath.replace("\\", "/")
            files.append(filepath)

    # loop through all files
    data = []
    name = []
    for file in files:
        # decompose filename
        name.append(file[:-4].split(f"{args.dir}/")[-1].split(args.split_name))
        # check if data is empty
        temp_data = np.loadtxt(file, skiprows=1, delimiter=",")
        if len(temp_data) < 1:
            temp_data = np.zeros(len(data[0]))
        # add data to list
        data.append(temp_data)

    # concatenate arrays
    name = np.array(name)
    data_clean = np.array(data)
    table = np.concatenate((name, data_clean), axis=1)

    # get headers
    headers_dir = args.head_subdirs.split(",")
    headers_data = pd.read_csv(files[0]).columns.to_list()
    headers = np.concatenate((headers_dir, headers_data))

    df = pd.DataFrame(table, columns=headers)

    # save csv
    df.to_csv(args.outdir)

    print("Done")


if __name__ == '__main__':
    args = parser()
    main(args)