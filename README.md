# CAPPA: Consistent analysis of Action Potential PArameters for various types of cardiomyocytes

This algorithm automatically extracts action potential (AP) parameters from recordings of hiPSC-CMs, stimulated isolated native CMs, HL-1 cells and electrophysiological cardiac modeling.

![](images/AP_parameter.png)


## 1. Installation

Clone repository

	git clone https://gitlab.kit.edu/kit/ibt-public/cappa.git

Create virtual environment

	cd cappa
	python3 -m venv venv
	source venv/bin/activate
	pip3 install -r requirements.txt

## 2. Running analysis
	
	python3 ap_analysis.py --dir <file or directory> --outdir <directory to save results> 

## 3. Arguments

* `--dir` path to single recording / file or directory without file endings
* `--outdir` path to directory in which results are stored
* `--subdirs` iterate over all subdirectories [y/n] (default is 'n')
* `--fileending` specify file ending (e.g. .abf or .dat, default is '.abf')
* `--delimiter` specify delimiter of input file if you don't use .abf or .dat (default is ' ')
* `--sample_freq` specify sample frequency in Hz (default is 1000 Hz)
* `--filter` specify if you want to apply filtering to your signal [y/n] (default is 'y') (recommendation: only use 'n' for smooth traces, e.g. obtained from in-silico modeling)
* `--window` specify if you want to use a window of a certain duration for averaging (for further details, see 6.) [y/n] (default is 'n')
* `--win_time` specify window duration in s (default is 10)
* `--overwrite` specify if you want to overwrite result files if they already exist [y/n] (default is 'n')
* `--apd` specify at which repolarization stages you want to calculate the APD separated by ',' (default is '30,40,50,60,90')
* `--peak` specify threshold potential in mV above which an AP is considered as an AP (default is -80)
* `--volt_change` defines the threshold of potential rise within 25 ms to detect APs (Caution, only change this parameter if you have exceptionally slow upstroke velocities!) (default is 20)
* `--plot` specify if you want to plot the results [y/n] (default is 'n')
* `--dpi` specify resolution of saved plot (default is 100)


## 4. Data preprocessing

To achieve reliable analyses of the maximum upstroke velocity and peak potential, the sampling rate should be at least fs = 2 kHz (rather use fs = 5 kHz or fs = 10 kHz). If args.filter is not set to 'n', the data is filtered with a 2nd order Butterworth lowpass filter at 1 kHz.

## 5. Analyzed AP parameters

* maximum upstroke velocity (in mV/ms)
* peak potential (in mV)
* maximum diastolic potential (MDP, in mV)
* AP amplitude (in mV)
* AP duration (APD, in ms)
* AP duration corrected by Bazett's formula for varying beating frequencies (cAPD, in ms)
* area APD<sub>90</sub>
* repolarization time (in ms)
* cycle length (in ms)
* frequency (in Hz)

### 5.1 Maximum upstroke velocity

To identify rapid depolarization phases, sequences characterized by a potential rise exceeding 20 mV within a time frame of less than 25 ms are detected. These time intervals are extended to 100 ms and the maximum upstroke velocity is extracted.

### 5.2 Peak potential

The peak potential is defined as the greatest potential between two consecutive upstrokes.

### 5.3 Maximum diastolic potential (MDP)

The MDP is defined as the lowest potential between two consecutive peaks.

### 5.3 AP amplitude

The AP amplitude is defined as the difference between a peak potential and its subsequent MDP.

### 5.4 AP duration (APD)

The start of an AP is defined by the intersection of a line fitted to the upstroke and one to the late diastolic depolarization phase. The APD is calculated by the time interval between the start of an AP and the first event after a peak at which the potential is lower than the respective amount of repolarization, e.g. 90 % (considering the AP amplitude).

### 5.5 Corrected AP duration (cADP)

The cAPD is defined by Bazett's formula for varying beating frequencies: $$ cAPD = \sqrt{\frac{APD}{cycle\ length}} $$

### 5.6 Area APD<sub>90</sub>

The area APD<sub>90</sub> is defined by the area between the curve of the membrane potential and the potential at 90 % repolarization (limited by the start of the AP). 

### 5.7 Time to peak

The time to peak is defined by the time interval between the peak and the start of an AP.

### 5.8 Repolarization time

The repolarization time is defined by the time interval between a peak and its subsequent MDP.

### 5.9 Cycle length

The cycle length is defined by the time interval between two consecutive AP starts.

### 5.10 Frequency

The frequency is defined by the inverse of the cycle length.


## 6. Averaging

The AP parameters from one recording are averaged by either
* considering the entire recording and capping the lowest and highest 10 % of all values (suitable for triggered APs)
* or by confining the mean calculation to a certain time interval of a user-defined duration which is automatically set to the sequence with the lowest variance in cycle length (suitable if hiPSC-CMs only show a short limit cycle).


## License

See [LICENSE](LICENSE)