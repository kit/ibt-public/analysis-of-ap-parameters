import os
import os.path
import pyabf
import matplotlib.pyplot as plt
import numpy as np
import argparse
import pandas as pd
from scipy.integrate import simpson
from scipy import stats
from scipy import signal
from matplotlib.lines import Line2D
import time

def parser():
    parser = argparse.ArgumentParser(description='Analysis of AP recordings')
    parser.add_argument('--dir',
                        type=str,
                        default="",
                        help='path to directory or single file')
    parser.add_argument('--outdir',
                        type=str,
                        default="",
                        help='path to outdir')
    parser.add_argument('--subdirs',
                        type=str,
                        default='n',
                        choices=['n', 'y'],
                        help='iterate over all subdirectories')
    parser.add_argument('--fileending',
                        type=str,
                        default=".abf",
                        help='specify fileending (e.g. .abf)')
    parser.add_argument('--delimiter',
                        type=str,
                        default=" ",
                        help="specify delimiter if you don't use .abf as fileending")
    parser.add_argument('--sample_freq',
                        type=float,
                        default=1000,
                        help="specify sample frequency in Hz")
    parser.add_argument('--filter',
                        type=str,
                        default="y",
                        choices=["y", "n"],
                        help="apply filtering to signal")
    parser.add_argument('--window',
                        type=str,
                        default="n",
                        choices=['n', 'y'],
                        help='use window to find steady state')
    parser.add_argument('--win_time',
                        type=int,
                        default=10,
                        help='define time of window for steady state')
    parser.add_argument('--overwrite',
                        type=str,
                        default='n',
                        choices=['n', 'y'],
                        help='procedure if file already exists (overwrite or proceed with next file)')
    parser.add_argument('--apd',
                        type=str,
                        default='30,40,50,60,90',
                        help='specify action potential durations in percent, separated by a comma (e.g. 60,90)')
    parser.add_argument('--peak',
                        type=float,
                        default=-80,
                        help="threshold potential above which an AP is considered as AP")
    parser.add_argument('--min',
                        type=str,
                        default="abs",
                        choices=["abs", "first"],
                        help="define whether MDP should be calculated as real MDP or as the first minimum before a"
                             "potential rise of 15 mV")
    parser.add_argument('--volt_change',
                        type=float,
                        default=20,
                        help="define threshold of potential rise within 25 ms to detect APs")
    parser.add_argument('--plot',
                        type=str,
                        default='n',
                        choices=['n', 'y'],
                        help='show plot')
    parser.add_argument('--dpi',
                        type=int,
                        default=100,
                        help='specify resolution')
    return parser.parse_args()


def main(args):
    t_start = time.time()
    # check user input arguments
    args = check_args(args)

    # find all files that are to be analyzed
    files = find_files(args)

    # initialize count of processed files
    count = 1
    # initialize count to reset errorfiles in new run
    err_no = 0
    # initialize time for analyzing one file (updated after each iteration)
    t_iter = 0.6
    runtimes = []
    err = ''

    # create plot
    fig = plt.figure(figsize=(16, 9))
    # turn interactive mode off
    plt.ioff()

    # start analysis
    for filenum, f in enumerate(files):
        # compute expected remaining time
        t_start_iter = time.time()
        time_rem = (len(files) - (count - 1)) * t_iter
        # calculate minutes and seconds
        time_rem_min = int(np.floor(time_rem / 60))
        time_rem_sec = round(time_rem - time_rem_min * 60, 1)

        # format spacing of printed statements in console
        # before file count, between file count and filename, between filename and time remaining, until filename
        space_cons = [(len(str(len(files))) - len(str(count))) * " ",
                      3 * " ",
                      (len(max(files, key=len)) - len(f) + 3) * " ",
                      (len(str(len(files))) * 2 + 2 + 3) * " "]

        # status information for user
        print(f"\n{space_cons[0]}{count}/{len(files)}:{space_cons[1]}Processing {f},{space_cons[2]}time remaining: "
              f"{time_rem_min} min {time_rem_sec} sec")

        # start analysis
        try:
            # prepare data
            err = 'prep'
            outfilename, data, data_filt, stim_idx, sample_freq, run_analysis = prepare_data(args, f, space_cons)

            # skip file if result file already exists and there is no overwrite permission
            if run_analysis == 0:
                print(f"{space_cons[3]}File {outfilename} already exists. \n{space_cons[3]}Analysis is not "
                      f"performed, since there is no permission to overwrite file. Check input argument (--overwrite).")
                count += 1
                continue

            # compute upstroke velocity
            err = 'upstroke'
            up_vel = upstroke_velocity(data, data_filt, stim_idx, sample_freq)

            # compute peak and minimum
            err = 'peak_min'
            up_vel, peak, mina = peak_min(data_filt, up_vel, sample_freq, space_cons)

            # compute apd
            err = 'apd'
            start, end, apd, v_cut = compute_apds(args, data_filt, up_vel, peak, mina, sample_freq)

            # compute remaining parameters
            err = 'rem'
            start, end, up_vel, peak, mina, v_cut, apd, time_to_peak, time_apd90_start, cycle_length, time_repo,\
                area_apd90, freq, capd, amp = \
                additional_parameters(data_filt, start, end, up_vel, peak, mina, v_cut, apd, sample_freq, stim_idx)

            # create and save plot
            err = 'plot'
            plot_fig(args, outfilename, data, data_filt, start, end, v_cut, apd, peak, mina, up_vel, sample_freq, stim_idx,
                 fig, space_cons=space_cons)

            # save parameters
            err = 'save'
            save_parameters(args, up_vel, peak, mina, apd, time_to_peak, time_apd90_start, cycle_length, time_repo,
                            area_apd90, freq, capd, amp, outfilename, data, data_filt, start, end, v_cut, sample_freq,
                            space_cons, stim_idx, fig)

            count += 1

            # calculate remaining time
            t_end_iter = time.time()
            current_runtime = round(t_end_iter - t_start_iter, 3)
            runtimes.append(current_runtime)
            t_iter = np.mean(runtimes)

        except:
            print(f"{space_cons[3]}Error in {err}")

            # create directory for error files
            errdir = f"{args.outdir}/error"

            # check if directory for error files already exists
            if not os.path.exists(os.path.join(errdir)):
                # create directory
                os.makedirs(os.path.join(errdir))
            elif err_no == 0:
                # remove all files in directory (if the first error in a new run occurs)
                for err_file in os.listdir(errdir):
                    # skip files starting with a dot (for Mac users)
                    if not err_file.startswith("."):
                        os.remove(os.path.join(errdir, err_file))
            err_no += 1

            # get errorfilename
            errfilename = outfilename.split("/")[-1]

            # write txt with all error files at the beginning
            fname = os.path.join(errdir, "0_error_files.txt")
            f = open(fname, 'a')
            f.write(f'{errfilename}: {err}.\n')
            f.close()

            # print path to errorfile
            print(f"{space_cons[3]}Error was written to log in {fname}")

            # plot files if data was loaded
            if not err == 'prep':
                plot_err(data, errdir, errfilename, space_cons, fig)

            count += 1

    # get total time for executing the script
    t_end = time.time()
    t_tot = t_end - t_start
    # get total time in min
    t_tot_min = int(np.floor(t_tot / 60))
    t_tot_sec = round(t_tot - t_tot_min * 60, 1)
    # print total time for executing the script
    print(f"\nDuration: {t_tot_min} min {t_tot_sec} sec\n")


def check_args(args):
    # check if file ending, input and output arguments are given
    if args.dir == "":
        args.dir = input("\nPlease specify input file or directory: ")
    if args.outdir == "":
        args.outdir = input("\nPlease specify output directory: ")
    if args.fileending == "":
        args.fileending = input("\nPlease specify file ending (e.g. .abf): ")

    # allow \ and / as directories in args
    if "\\" in args.dir:
        args.dir = args.dir.replace("\\", "/")
    if "\\" in args.outdir:
        args.outdir = args.outdir.replace("\\", "/")

    # check if directory already exists
    if os.path.exists(os.path.join(args.outdir)):
        print("\nOutput directory already exists. Files will be appended.")
    else:
        # create directory
        os.makedirs(os.path.join(args.outdir))
        print("\nOutput directory created.")

    input("\nFor well formatted plots never close the plotting window! Follow the instructions in the terminal."
          "\nPress <Enter> to start analysis.")

    return args


def find_files(args):
    files = []
    if args.subdirs != 'n':
        # find all files ending with specified file ending (and walk through all subdirectories)
        for dirpath, dirnames, filenames in os.walk(args.dir):
            for filename in [f for f in filenames if f.endswith(args.fileending)]:
                filepath = os.path.join(dirpath, filename)
                if "\\" in filepath:
                    filepath = filepath.replace("\\", "/")
                files.append(filepath)
    else:
        files.append(args.dir + args.fileending)

    return files


def prepare_data(args, f, space_cons):
    # store initial filename
    file = f
    # check for points in name
    pts_in_name = (args.outdir + f).count(".")
    # remove directory from filename if iterated through all subdirectories
    if args.subdirs == 'y':
        f = f.split(f'{args.dir}/')[-1]
    # define output filename
    if pts_in_name > 1:
        outfilename = "_".join((args.outdir + "/" + f.replace("/", "_").replace(":", "").replace(" ", "")).split(".")[:-1])
    else:
        outfilename = (args.outdir + "/" + f.replace("/", "_").replace(":", "").replace(" ", "")).split(".")[0]
    # check if files already exist
    if os.path.exists(outfilename + ".png") or os.path.exists(outfilename + ".csv"):
        # skip file if there is no overwrite permission
        if args.overwrite == 'n':
            return outfilename, [], [], [], args.sample_freq, 0
        elif args.overwrite == 'y':
            print(f"{space_cons[3]}Overwrite file {outfilename}")

    # define sampling frequency
    sample_freq = args.sample_freq

    # import data
    if file.split(".")[-1] == "abf":
        temp_data = pyabf.ABF(file)
        # get membrane potential
        y_data = temp_data.data[0, :]

        # get sample frequency
        if hasattr(temp_data, 'sampleRate'):
            sample_freq = temp_data.sampleRate

        # search for stimuli
        # check if stimulus exists
        if len(temp_data.sweepC[temp_data.sweepC > temp_data.sweepC[0]]) > 1:
            try:
                # compute half of mean of stimulus strength in last sweep
                stim_val = np.nanmean(temp_data.sweepC[temp_data.sweepC > temp_data.sweepC[0]]) * 1 / 2
                # find stimuli in data with a threshold of 1/2 of the mean stimulus strength of the last stimulus sequence
                stim_idx_all = np.where(temp_data.data[1, :] > stim_val)[0]
                # keep first and last stimulus index for each stimulus sequence (find differences in neighboring values > 1)
                stim_idx_diff_ind = np.where(np.diff(stim_idx_all) != 1)[0]
                # append last stimulus index
                stim_idx_last = np.append(stim_idx_all[stim_idx_diff_ind], stim_idx_all[-1])
                # append first stimulus index
                stim_idx_first = np.append(stim_idx_all[0], stim_idx_all[stim_idx_diff_ind + 1])
                stim_idx = np.array([stim_idx_first, stim_idx_last])
            except:
                stim_idx = []
                print(f"{space_cons[3]}No stimulus index found.")
        else:
            stim_idx = []
            print(f"{space_cons[3]}No stimulus index found.")

    elif file.split(".")[-1] == "dat":
        # output file from Courtemanche model in openCARP
        y_data = np.loadtxt(file, delimiter="\t", usecols=16)
        # initialize empty stimuli indices
        stim_idx = []

    else:
        try:
            # load data
            data = np.loadtxt(file, delimiter=args.delimiter, dtype=float)
            # get shape of data
            shape_data = data.shape
            if shape_data[0] < shape_data[1]:
                y_data = data[1, :]
            else:
                y_data = data[:, 1]
            # initialize empty stimuli indices
            stim_idx = []

        except:
            print(f"{space_cons[3]}Data cannot be loaded.")
            exit()

    # define x-values with frequency
    x_data = np.arange(0, len(y_data) / sample_freq, 1 / sample_freq)

    # filter data
    if args.filter == "y":
        if sample_freq > 2000:
            # create lowpass filter of 2nd order at a cutoff frequency of 1 kHz
            b_lp1000, a_lp1000 = signal.butter(2, 1000, btype="low", output="ba", fs=sample_freq)
            # get filtered signal
            y_data_filt = signal.filtfilt(b_lp1000, a_lp1000, y_data)
        else:
            # don't apply filtering
            print(f"{space_cons[3]}Filtering is not applied as sample frequency is too low. Needs to be at least "
                  f"at 2000 Hz.")
            y_data_filt = y_data
    else:
        # don't apply filtering
        y_data_filt = y_data

    # construct data array
    data = np.array([x_data, y_data])
    data_filt = np.array([x_data, y_data_filt])

    return outfilename, data, data_filt, stim_idx, sample_freq, 1


def upstroke_velocity(data, data_filt, stim_idx, sample_freq):
    # extract data
    x = data[0, :]
    # either use filtered or raw data
    if args.filter == "n" or sample_freq < 2000:
        # use raw data if no filtering is desired or if sampling frequency is too low
        y = data[1, :]
    else:
        y = data_filt[1, :]

    # search for sequences with a potential change exceeding 20 mV in a defined rolling window of 25 ms
    win_time = int(25 * sample_freq / 1000)
    # get rolling window view (window is placed left: considering the following values)
    rolling_window_view = np.lib.stride_tricks.sliding_window_view(y, window_shape=(win_time,))
    
    # get maximum and minimum values in each window (row)
    y_max = np.max(rolling_window_view, axis=1)
    y_min = np.min(rolling_window_view, axis=1)
    
    # upstroke if potential change is more than defined threshold (20 mV) in less than defined window duration (25 ms)
    # and if maximum potential is after minimum potential (to not extract repolarization sequences)
    high_diffs = np.where(y_max - y_min > args.volt_change)[0]

    # get indices of max and min
    y_argmax = np.argmax(rolling_window_view[high_diffs], axis=1)
    y_argmin = np.argmin(rolling_window_view[high_diffs], axis=1)

    # keep windows in which maximum is after minimum (positive potential change)
    high_diffs_pos = high_diffs[y_argmax - y_argmin > 0]

    # get greatest slope and remove other values in a window of 0.1 s
    win_search = int(100 * sample_freq / 1000)
    # calculate upstroke velocities in mV/ms
    dy = (y[1:] - y[:-1]) / (x[1] - x[0]) / 1000
    up_vel_val = []
    up_vel_arg = []
    while len(high_diffs_pos) > 0:
        i = high_diffs_pos[0]
        # define window for upstroke velocity search
        cand = np.arange(i, i + win_search, 1, dtype=int)
        indices = np.arange(0, len(dy), 1)
        # intersect all indices from window with all possible indices (to avoid out of bounds indexing)
        cand = np.intersect1d(cand, indices)
        # start search after stimuli if present in current search window
        if len(stim_idx) > 0:
            # check if stimulus is in identified window for upstroke
            stim_in_win = np.intersect1d(stim_idx[1, :], cand)
            if len(stim_in_win) > 0:
                # restrict search to sequence after stimulus artefact
                cand = cand[cand > stim_in_win[0]]
        # find greatest slope and corresponding index
        up_vel_val.append(np.max(dy[cand]))
        up_vel_arg.append(int(np.argmax(dy[cand]) + cand[0]))
        # remove processed indices
        high_diffs_pos = np.setdiff1d(high_diffs_pos, np.arange(i, i + win_search, 1))

    up_vel = np.array([up_vel_arg, up_vel_val])

    return up_vel


def peak_min(data_filt, up_vel, sample_freq, space_cons):
    y_filt = data_filt[1, :]
    up_vel_arg = np.array(up_vel[0, :], dtype=int)

    peak_val = []
    peak_arg = []
    min_val = []
    min_arg = []

    # search for peaks (between consecutive upstroke velocities)
    for i, ind in enumerate(up_vel_arg):
        # time window for peak search (limited by cycle length)
        win_peak = int(100 * sample_freq / 1000)
        # get peak values and indices
        peak_val.append(np.nanmax(y_filt[ind:ind+win_peak]))
        peak_arg.append(np.nanargmax(y_filt[ind:ind+win_peak]) + ind)

        # skip further analysis if peak is lower than defined value
        if peak_val[-1] < args.peak:
            # set peak and upstroke velocity to nan (to visualize if stimulus doesn't trigger an AP)
            peak_val[-1] = np.nan
            peak_arg[-1] = -1  # needs to ba an integer
            up_vel[0, i] = -1  # needs to be an integer
            up_vel[1, i] = np.nan
            continue

    # compute minima
    for i, ind in enumerate(peak_arg):
        # set value to nan if no peak was found (peak was lower than defined threshold)
        if ind == -1:
            min_val.append(np.nan)
            min_arg.append(-1)  # needs to be an integer
            continue

        # get minimum values and indices (between peaks / upstroke velocities)
        if i == len(peak_arg) - 1:
            # search till end after last AP
            index = len(y_filt)
        elif args.min == "abs":
            # search until the next peak
            index = peak_arg[i+1]
        else:
            # find first local minimum (first minimal value before a potential rise of 15 mV)
            # accumulate minimum values until next peak (always save smallest value until index)
            min_vals = np.minimum.accumulate(y_filt[ind:peak_arg[i+1]])
            # compute differences between values and minimum values until index
            diffs = y_filt[ind:peak_arg[i+1]] - min_vals
            # find first index that is 15 mV higher than minimum value until index (local "maximum")
            indices = ind + np.where(diffs > 15)[0]

            if len(indices) < 1:
                # update index to search until next peak if no local maximum was found
                index = peak_arg[i+1]
            else:
                # choose first index
                index = indices[0]

        # find minima
        min_val.append(np.nanmin(y_filt[ind:index]))
        min_arg.append(np.nanargmin(y_filt[ind:index]) + ind)

    peak = np.array([peak_arg, peak_val])
    mina = np.array([min_arg, min_val])

    # check if the first and the last AP are entirely recorded
    # check if  APs were detected at all
    if np.sum(~np.isnan(peak_val)) > 0:
        # check start: get first peak
        # -> if there is a value lower than the mean cutoff voltage of APD90 before peak: pass / else: skip first peak
        # get cutoff voltage for APD90 of first AP
        temp_cutoff = np.nanmean(peak_val) - 0.9 * (np.nanmean(peak_val) - np.nanmean(min_val))
        # check if APD90 voltage is found before first peak
        if not np.any(y_filt[:peak_arg[0]] < temp_cutoff):
            # remove first upstroke velocity and peak and keep minium (always start with a minimum due to computation of
            # starting point of apd)
            peak = peak[:, 1:]
            up_vel = up_vel[:, 1:]
        else:
            # always start with a minimum due to computation of starting point of apd
            min_val0 = np.nanmin(y_filt[:int(peak[0, 0])])
            min_arg0 = np.nanargmin(y_filt[:int(peak[0, 0])])
            mina = np.insert(mina, 0, [min_arg0, min_val0], axis=1)

        # check end: check if last minimum is a real local minimum (range comparable to previous data)
        # get threshold to check if last AP was entirely recorded (not greater than 5 mV of mean or
        # greater than mean + 2 SD)
        thresh_var = np.max([5, np.nanstd(min_val)])
        if min_arg[-1] == 0 or min_val[-1] > np.nanmean(min_val) + thresh_var:
            # remove last upstroke velocity, peak and minium
            peak = peak[:, :-1]
            mina = mina[:, :-1]
            up_vel = up_vel[:, :-1]

    if np.sum(~np.isnan(peak[1, :])) == 0:
        # throw an error if no APs were detected
        print(f"{space_cons[3]}!!! No APs were detected. Skip further analysis. "
              f"Try to change args.peak or args.volt_change. !!!")
        raise ValueError

    return up_vel, peak, mina


def compute_apds(args, data_filt, up_vel, peak, mina, sample_freq):
    # extract values
    y_filt = data_filt[1, :]
    up_vel_arg = np.array(up_vel[0, :] , dtype=int)
    up_vel_val = up_vel[1, :] * 1000 / sample_freq
    peak_arg = np.array(peak[0, :], dtype=int)
    peak_val = peak[1, :]
    min_arg = np.array(mina[0, :], dtype=int)
    min_val = mina[1, :]

    # initialize apds
    apds_init = np.array(args.apd.split(','), dtype=int)

    # compute amplitudes
    amp_tot = peak_val - min_val[1:]

    # initialize empty arrays for start and end of apds
    start = []
    end_dict = dict()
    vcut_dict = dict()
    for i in apds_init:
        end_dict[i] = []
        vcut_dict[i] = []

    # use intersection of fitted line in upstroke and horizontal line through MDP
    for i, ind in enumerate(min_arg[:-1]):
        # skip APD calculation if AP is not considered as AP (peak too small)
        if np.isnan(peak[1, i]):
            start.append(-1)  # needs to be an integer
            for j in apds_init:
                vcut_dict[j].append(np.nan)
                end_dict[j].append(-1)  # needs to be an integer
            continue

        # line in max. upstroke (first value is slope, second value y-axis intercept)
        temp_up = np.array([up_vel_val[i], y_filt[up_vel_arg[i]] - up_vel_val[i] * up_vel_arg[i]])

        # line in MDP (slope 0)
        temp_base = [0, y_filt[ind]]

        # intersection of lines
        start_temp = int(np.floor((temp_base[1] - temp_up[1]) / (temp_up[0] - temp_base[0])))
        start.append(start_temp)

        for j in apds_init:
            # get ends of apds
            temp_vcut = peak_val[i] - amp_tot[i] * j / 100
            vcut_dict[j].append(temp_vcut)
            temp_end = int(np.where(y_filt[peak_arg[i]:] <= temp_vcut)[0][0] + peak_arg[i])
            end_dict[j].append(temp_end)

    # convert values from dict to array
    end = np.array(list(end_dict.values()))
    v_cut = np.array(list(vcut_dict.values()))

    # convert list to array
    start = np.array(start)

    # compute APDs in ms
    apd = (end - start) / sample_freq * 1000

    # set entries for APs that were not considered as AP to nan
    apd[:, start == -1] = np.nan

    return start, end, apd, v_cut


def additional_parameters(data_filt, start, end, up_vel, peak, mina, v_cut, apd, sample_freq, stim_idx):
    # extract data
    x = data_filt[0, :]
    y_filt = data_filt[1, :]

    # remove all nans if no stimulus index was found (for arguments '-1' is used instead of nan)
    # (nans are only required for plotting if stimulus is given but does not trigger an AP)
    if len(stim_idx) == 0:
        # keep all values that are not -1
        start = start[start != -1]
        # keep all values that do not have a -1 in their column
        end = end[:, np.all(end != -1, axis=0)]
        # keep all values that do not have a nan in their column
        apd = apd[:, ~np.isnan(apd).any(axis=0)]
        # keep all values that do not have a nan in their column
        up_vel = up_vel[:, ~np.isnan(up_vel).any(axis=0)]
        peak = peak[:, ~np.isnan(peak).any(axis=0)]
        mina = mina[:, ~np.isnan(mina).any(axis=0)]
        v_cut = v_cut[:, ~np.isnan(v_cut).any(axis=0)]

    # create mask to set entries for APs that were not considered as AP to nan
    mask_aps = np.where(start == -1)[0]
    # create mask for values that are calculated between APs
    mask_aps_betw1 = np.where(start[:-1] == - 1)[0]
    # create mask that also sets previous entries to nan as some values are calculated between APs
    # (shift indices by -1 and only keep positive indices by skipping first element)
    mask_aps_betw2 = np.where(start[1:] == -1)[0]
    # concatenate masks
    mask_aps_betw = np.unique(np.concatenate((mask_aps_betw1, mask_aps_betw2)))

    ''' time to peak '''
    time_to_peak = (peak[0, :] - start) / sample_freq * 1000
    # set entries for APs that were not considered as AP to nan
    time_to_peak[mask_aps] = np.nan

    ''' time between apd90 and start of next AP '''
    time_apd90_start = (start[1:] - end[-1, :-1]) / sample_freq
    # set entries for APs that were not considered as AP to nan
    time_apd90_start[mask_aps_betw] = np.nan

    ''' cycle length '''
    cycle_length = (np.array(start[1:]) - np.array(start[:-1])) / sample_freq
    # set entries for APs that were not considered as AP to nan
    cycle_length[mask_aps_betw] = np.nan

    ''' repolarization time '''
    time_repo = (mina[0, 1:] - peak[0, :]) / sample_freq * 1000
    # set entries for APs that were not considered as AP to nan
    time_repo[mask_aps] = np.nan

    ''' area of apd90 '''
    area_apd90 = np.zeros(len(start))
    for i, start_ind in enumerate(start):
        if start_ind > end[-1, i] or start_ind == -1:
            # skip computation if start is past end or if AP is not considered as AP (peak potential below threshold)
            area_apd90[i] = np.nan
            continue
        # subtract apd90 potential and calculate area under the ap curve
        area_apd90[i] = simpson(y_filt[start[i] : end[-1, i]] - v_cut[-1, i], x[start[i] : end[-1, i]])

    ''' frequency '''
    freq = 1 / cycle_length

    ''' APD corrected with Bazett's forumla '''
    capd = apd[:, 1:] / np.sqrt(cycle_length)

    ''' amplitude '''
    amp = peak[1, :] - mina[1, 1:]

    return start, end, up_vel, peak, mina, v_cut, apd, time_to_peak, time_apd90_start, cycle_length, time_repo,\
        area_apd90, freq, capd, amp


def plot_fig(args, outfilename, data, data_filt, start, end, v_cut, apd, peak, mina, up_vel, sample_freq, stim_idx, fig,
         win_indices=None, space_cons=None):
    # clear content of figure
    plt.clf()
    # extract values
    x = data[0, :]
    y = data[1, :]
    y_filt = data_filt[1, :]
    up_vel_arg = np.array(up_vel[0, :], dtype=int)

    # prepare arrays
    start_tile = np.tile(start,(end.shape[0],1))
    start_flat = start_tile.flatten()
    end_flat = end.flatten()
    v_cut_flat = v_cut.flatten()

    # set alpha value for plotting depending on the presence or absence of window averaging
    if win_indices is None:
        alpha_marker = 1
    else:
        alpha_marker = 0.3

    # plot sequence for each stimulus separately, if stimulus is given
    if len(stim_idx) > 0:
        # extract number of stimuli (subplots)
        n_plots = len(peak[0, :])
        # get maximum APD90 in seconds and round fourth of it to an even number with two digits (two decimals)
        apd90_max = np.ceil(np.nanmax(apd[-1, :]) / 1000 * 100 / 4) * 4 / 100
        # calculate x ticks for each subplot
        xticks = np.arange(0, apd90_max + apd90_max / 4, apd90_max / 4)
        # calculate displayed x-limit
        xlim = [round(-apd90_max / 8, 4), round(9/8 * apd90_max, 4)]
        # get indices to plot (first and last index limited by 0 and last index)
        plot_ind = np.array([np.maximum(np.floor(start - apd90_max / 8 * sample_freq), 0),
                            np.minimum(np.ceil(start + 9/8 * apd90_max * sample_freq + 1), len(x) - 1)], dtype=int)
        # define x shift that 0 is set to start of APD
        x_shift = start / sample_freq
        # define intervals for y-ticks
        y_intvls = 20
        # define number of subplots
        cols = 5
        rows = int(np.ceil(n_plots / cols))
        # define values for formatting
        fontsize = 14
        fontweight = "normal"
    else:
        # only 1 plot
        n_plots = 1
        # define x-ticks
        x_interval = np.ceil(max(x) / 30)
        xticks = np.arange(0, max(x) + x_interval, x_interval)
        # define displayed x-limit
        xlim = [0, max(x) + 0.001]
        plot_ind = np.array([[0], [len(x)]], dtype=int)
        x_shift = [0]
        # define intervals for y-ticks
        y_intvls = 10
        # define number of subplots
        cols = 1
        rows = 1
        # define values for formatting
        fontsize = 18
        fontweight = "bold"

    # configure figure
    fig.subplots_adjust(left=0.07, right=0.98, top=0.88, bottom=0.08, wspace=0.08, hspace=0.1)

    for i in range(n_plots):
        # plot data (max. 5 plots per row)
        ax = fig.add_subplot(rows, cols, i + 1)

        # define plotting range
        plot_range = np.arange(plot_ind[0, i], plot_ind[1, i])
        plt.plot(x[plot_range] - x_shift[i], y[plot_range], label="raw data", alpha=0.5)
        plt.plot(x[plot_range] - x_shift[i], y_filt[plot_range], label='filtered data', color="tab:blue")

        # skip visualization if no AP was detected or if no AP was triggered
        if peak.shape[1] == 0:
            continue
        if np.isnan(peak[1, i]):
            continue

        # no slicing if only one plot (j1 is j+1)
        j, j1 = (i, i + 1) if n_plots > 1 else (slice(None), slice(1, None))

        # plot starts
        plt.plot(x[start[j]] - x_shift[i], y_filt[start[j]], linestyle="none", marker="o", markerfacecolor="none",
                 markersize=10, markeredgewidth=1.5, label="AP start", color="#77AC30", alpha=alpha_marker)

        # plot event of upstroke velocity
        plt.plot(up_vel_arg[j] / sample_freq - x_shift[i], y_filt[up_vel_arg[j]], linestyle='none', marker='o',
                 markerfacecolor="none", markersize=10, markeredgewidth=1.5, label="max. upstroke velocity",
                 color="#00ADFF", alpha=alpha_marker)

        # plot peaks
        plt.plot(peak[0, j] / sample_freq - x_shift[i], peak[1, j], linestyle='none', marker='o', markerfacecolor="none",
                 markersize=10, markeredgewidth=1.5, label='peak potential', color="#FF674A", alpha=alpha_marker)

        # plot minima only if one single plot is shown, otherwise draw dashed line
        if n_plots == 1:
            # plot marker
            plt.plot(mina[0, :] / sample_freq, mina[1, :], linestyle='none', marker='o', markerfacecolor="none",
                     markersize=10, markeredgewidth=1.5, label='max. diastolic potential', color="#F8C262",
                     alpha=alpha_marker)
        else:
            # plot line
            plt.plot([x[plot_ind[0, i]] - x_shift[i], x[plot_ind[1, i]] - x_shift[i]], [mina[1, j1], mina[1, j1]],
                     linestyle="dashed", linewidth=1.5, label="max. diastolic potential", color="#F8C262")

        # adapt slicing for plotting
        # (plot only 1 APD with specific number of repolarization stages (30, 40, 50...) if multiple plots are shown)
        k = np.arange(i, len(start_flat), len(apd[0, :])) if n_plots > 1 else slice(None)
        # plot apds
        # plot horizontal lines
        plt.plot([x[start_flat[k]] - x_shift[i], x[end_flat[k]] - x_shift[i]], [v_cut_flat[k], v_cut_flat[k]],
                 color='black', linestyle='dashed', alpha=alpha_marker)
        # plot vertical lines
        plt.plot([x[start[j]] - x_shift[i], x[start[j]] - x_shift[i]], [mina[1, j1], peak[1, j]], color='black',
                 linestyle='dashed', alpha=alpha_marker)

        # get min and max values (round to values that can be divided by 10)
        y_min = np.floor(np.nanmin(y_filt) / 10) * 10
        y_max = np.ceil(np.nanmax(y_filt) / 10) * 10
        # if the max value is not a multiple of the defined interval size starting from y_min, add 10 (as interval
        # sizes are either 10 or 20)
        if (y_max - y_min) % y_intvls != 0:
            y_max += 10

        # format figure
        plt.grid(True)
        plt.xticks(xticks, fontsize=14)
        plt.yticks(np.arange(y_min, y_max + y_intvls, y_intvls), fontsize=14)
        plt.ylim(y_min, y_max)
        plt.xlim(xlim)
        xlabel = plt.xlabel("Time in s", fontsize=fontsize, fontweight=fontweight, labelpad=10)
        ylabel = plt.ylabel("Voltage in mV", fontsize=fontsize, fontweight=fontweight, labelpad=10)

        if n_plots > 1 and i < rows * cols - cols:
            # don't show x-ticks and x-labels for upper rows (only for last row)
            xlabel.set_color("none")
            plt.xticks(color="none")
        if i not in np.arange(0, rows * cols, cols):
            # don't show y-ticks and y-labels for columns right of first column
            ylabel.set_color("none")
            plt.yticks(color="none")

    # format legend
    # add black line
    line_black = Line2D([0], [0], label='APDs', color='black', linestyle='dashed')
    handles, labels = ax.get_legend_handles_labels()
    handles.insert(2, line_black)
    # add placeholder
    line_white = Line2D([0], [0], label="", color="none")
    handles.insert(3, line_white)
    # for multiple plots MDP is plotted as line otherwise as marker -> sort legend accordingly
    if n_plots > 1:
        # swap invisible line with line for MDP
        handles[3], handles[-1] =handles[-1], handles[3]
    lgnd = fig.legend(handles=handles, bbox_to_anchor=(1, 1), ncol=2)

    # add title
    fig.suptitle("Analysis of AP parameters", y=0.96, fontsize=20, fontweight='bold')

    # show plot
    if args.plot == 'y' and win_indices is None and args.window == "n":
        plt.draw()
        plt.show(block=False)
        input(f"{space_cons[3]}Press <Enter> to continue. ")
    elif win_indices is not None:
        ## plot window
        # get start and end indices of window
        t_win_start = start[win_indices[0]]
        t_win_end = start[win_indices[-1] + 1]

        # get min and max values to plot red window
        y_min_win = np.nanmin(y_filt)
        y_max_win = np.nanmax(y_filt)

        # plot vertical lines
        plt.plot(np.array([t_win_start, t_win_start]) / sample_freq, [y_min_win - 5, y_max_win + 5], color='red')
        plt.plot(np.array([t_win_end, t_win_end]) / sample_freq, [y_min_win - 5, y_max_win + 5], color='red')
        # plot horizontal lines
        plt.plot(np.array([t_win_start, t_win_end]) / sample_freq, [y_min_win - 5, y_min_win - 5], color='red')
        plt.plot(np.array([t_win_start, t_win_end]) / sample_freq, [y_max_win + 5, y_max_win + 5], color='red')

        # plot parameters in window
        # plot starts
        plt.plot(x[start[win_indices]], y_filt[start[win_indices]], linestyle="none", marker="o",
                 markerfacecolor="none", markersize=10, markeredgewidth=1.5, label="AP start", color="#77AC30")

        # plot event of upstroke velocity
        plt.plot(up_vel_arg[win_indices] / sample_freq, y_filt[up_vel_arg[win_indices]], linestyle='none', marker='o',
                 markerfacecolor="none", markersize=10, markeredgewidth=1.5, label="max. upstroke velocity",
                 color="#00ADFF")

        # plot peaks and minima
        plt.plot(peak[0, win_indices] / sample_freq, peak[1, win_indices], linestyle='none', marker='o',
                 markerfacecolor="none", markersize=10, markeredgewidth=1.5, label='peak potential', color="#FF674A")
        plt.plot(mina[0, win_indices + 1] / sample_freq, mina[1, win_indices + 1], linestyle='none', marker='o',
                 markerfacecolor="none", markersize=10, markeredgewidth=1.5, label='max. diastolic potential',
                 color="#F8C262")

        # plot apds
        # make 2D array from start
        start_tile = np.tile(np.array(start), (len(end[:, 0]), 1))
        # flatten 2D arrays to 1D
        start_flat = start_tile[:, win_indices].flatten()
        end_flat = end[:, win_indices].flatten()
        v_cut_flat = v_cut[:, win_indices].flatten()
        # plot horizontal lines
        plt.plot([x[start_flat], x[end_flat]], [v_cut_flat, v_cut_flat], color='black', linestyle='dashed')
        # plot vertical lines
        plt.plot([x[start[win_indices]], x[start[win_indices]]], [mina[1, win_indices + 1], peak[1, win_indices]],
                 color='black', linestyle='dashed')

        # update limits for y-axis so that red window is in plotting range
        if y_min_win - 5 < y_min:
            # update lower limit
            y_min = y_min_win - 7
        if y_max_win + 5 > y_max:
            # update upper limit
            y_max = y_max_win + 7

        plt.ylim(y_min, y_max)

        # format legend and set alpha values of markers to 1
        for i in range(3, len(handles)):
            lgnd.legend_handles[i]._alpha = 1

        plt.draw()
        plt.show(block=False)

    # save plot
    fig.savefig(outfilename + ".png", dpi=args.dpi)


def save_parameters(args, up_vel, peak, mina, apd, time_to_peak, time_apd90_start, cycle_length, time_repo, area_apd90,
                    freq, capd, amp, outfilename, data, data_filt, start, end, v_cut, sample_freq, space_cons, stim_idx,
                    fig):
    # add all output values to a dict
    out = dict()
    out["max_up_vel (V/s)"] = up_vel[1, :]
    out["#peaks (1)"] = len(peak[1, :])
    out["peak (mV)"] = peak[1, :]
    out["min (mV)"] = mina[1, :]
    out["amp (mV)"] = amp
    for i, perc in enumerate(args.apd.split(",")):
        out[f"apd{perc} (ms)"] = apd[i, :]
    for i, perc in enumerate(args.apd.split(",")):
        out[f"capd{perc} (ms)"] = capd[i, :]
    out["time_to_peak (ms)"] = time_to_peak
    out["time_repo (ms)"] = time_repo
    out["time_apd90_start (s)"] = time_apd90_start
    out["cycle_length (s)"] = cycle_length
    out["area_apd90 (mVs)"] = area_apd90
    out["freq (Hz)"] = freq

    # compute output values
    out_final = dict()

    # use time window to determine steady state
    if args.window == 'y':
        # skip windowing if no APs (less than 3 APs) was detected
        if peak.shape[1] < 3:
            print(f"{space_cons[3]}Not enough APs detected to apply windowing.")
            return
        # windowing can't be applied for stimulated APs (doesn't make sense as there is no variance in cycle length)
        elif len(stim_idx) > 0:
            print(f"{space_cons[3]}Windowing cannot be applied for stimulated APs.")
        else:
            # initialize list with flags indicating if window is smaller or larger than desired for each iteration
            window_len = []
            # get estimated number of subsequent events
            n_in_win = int(np.mean(freq) * args.win_time)
            # iterate while window time is smaller than user-defined window time
            while True:
                # check if desired number of events is larger than number of events in recording
                # (desired window length is larger than recording)
                if n_in_win >= len(cycle_length):
                    n_in_win = len(cycle_length)
                    window_len.append("p")
                if n_in_win == 1:
                    n_in_win += 1
                    window_len.append("p")
                # get index with the smallest variance in cycle length
                idx = pd.Series(cycle_length).rolling(n_in_win, center=False).var().idxmin()
                # compute array with events in window
                idx_lst = np.arange(idx - (n_in_win - 1), idx + 1, 1)
                # left boundary of window is located at first start of AP in identified sequence
                t_first_idx = start[idx_lst[0]]
                # right boundary of window is located at last start of AP in identified sequence + 1
                t_last_idx = start[idx_lst[-1] + 1]
                # compute current time in window
                curr_wintime = t_last_idx - t_first_idx

                # update number of events in window
                if curr_wintime >= args.win_time * sample_freq:
                    # decrease number of events
                    n_in_win -= 1
                    # indicate that window was too large
                    window_len.append("l")
                elif curr_wintime < args.win_time * sample_freq:
                    # increase number of events
                    n_in_win += 1
                    # indicate that window was too small
                    window_len.append("s")

                # end iteration if window can't be enlarged
                if "p" in window_len:
                    break

                if len(window_len) > 1:
                    # stop iteration if window was too small before and is too large now
                    if window_len[-2] == "s" and window_len[-1] == "l":
                        break

            # indices of APs in window
            win_indices = idx_lst

            # plot results with window
            plot_fig(args, outfilename, data, data_filt, start, end, v_cut, apd, peak, mina, up_vel, sample_freq,
                     [], fig, win_indices)

            # get user input if window is correct
            inpt = []
            while inpt != "y" and inpt != "n":
                inpt = input(f"{space_cons[3]}Window correct? [y/n]: ")
                if inpt != "y" and inpt != "n":
                    print(f"{space_cons[3]}Please answer with 'y' or 'n' and press <Enter>.")

            # iteratively adapt window if user doesn't define it as correct
            while inpt != "y":
                # create dummy values
                t_first = 0
                t_last = -1
                while t_first > t_last:
                    try:
                        t_first = float(input(f"{space_cons[3]}Start of window: ")) * sample_freq
                        t_last = float(input(f"{space_cons[3]}End of window: ")) * sample_freq
                        if t_first < t_last:
                            # correct input -> quit loop
                            break
                        else:
                            print(f"{space_cons[3]}End of window must be larger than start of window.")
                    except ValueError:
                        print(f"{space_cons[3]}Enter valid floating point numbers.")
                    # reset values to continue loop if input was incorrect
                    t_first = 0
                    t_last = -1
                # get first start of AP after defined start time of window
                t_first_idx = start[start > t_first][0]
                # check if there is a new AP after the defined end time of the window
                if len(start[start > t_last]) > 1:
                    # get first start of AP after defined end time of window
                    t_last_idx = start[start > t_last][0]
                else:
                    # select last start of AP
                    t_last_idx = start[-1]
                # get events
                idx_first = np.where(start == t_first_idx)[0][0]
                idx_last = np.where(start == t_last_idx)[0][0]
                if idx_last == idx_first:
                    # make sure at least 1 AP is inside the selected window
                    idx_last = idx_first + 1
                # determine index list
                idx_lst = np.arange(idx_first, idx_last, 1)

                # indices of APs in window
                win_indices = idx_lst

                # plot results with window
                plot_fig(args, outfilename, data, data_filt, start, end, v_cut, apd, peak, mina, up_vel, sample_freq,
                         [], fig, win_indices)

                # reset previous input
                inpt = []

                # get user input
                while inpt != "y" and inpt != "n":
                    inpt = input(f"{space_cons[3]}Window correct? [y/n]: ")
                    if inpt != "y" and inpt != "n":
                        print(f"{space_cons[3]}Please answer with 'y' or 'n' and press [enter].")

    # compute mean and standard deviation of all values
    for name, val in out.items():
        val = np.array(val)
        # add nan value for parameters calculated between APs if only 1 AP was detected
        if val.size == 0:
            val = np.array([np.nan])
        # skip non-iteratable values
        if name == "#peaks (1)" and (args.window == 'n' or (args.window == 'y' and len(stim_idx) > 0)):
            # subtract number of peaks belonging to the lowest and highest 10 %
            out_final[name] = val - int(np.ceil(val * 0.1))
            continue
        elif name == "#peaks (1)" and args.window == 'y':
            out_final[name] = len(idx_lst)
            continue
        # use mean of time between peaks for frequency
        elif name == "freq (Hz)":
            out_final[name] = 1 / out_final["cycle_length_mean (s)"]
            continue

        # split names between description and unit
        varname, unit = name.split(" ")

        # windowing cannot be applied for stimulated APs (doesn't make sense as there is no variation in cycle length)
        if args.window == 'n' or (args.window == 'y' and len(stim_idx) > 0):
            # sort string and remove upper and lower 10 %
            val_sorted = np.sort(val)
            to_remove = int(np.ceil(len(val_sorted) * 0.1))
            if len(val_sorted) > 2:
                val_adapted = val_sorted[to_remove:-to_remove]
            else:
                val_adapted = val_sorted
        else:
            if 'start' in name:
                val_adapted = val[idx_lst[0] : idx_lst[-1] + 1]
            elif 'min' in name:
                val_adapted = val[idx_lst[0] + 1 : idx_lst[-1] + 3]
            else:
                val_adapted = val[idx_lst[0] : idx_lst[-1] + 2]
        # compute mean and std
        if np.sum(~np.isnan(val_adapted)) == 0:
            out_final[f"{varname}_mean {unit}"] = np.nan
            out_final[f"{varname}_std {unit}"] = np.nan
        else:
            out_final[f"{varname}_mean {unit}"] = round(np.nanmean(val_adapted), 3)
            out_final[f"{varname}_std {unit}"] = round(np.nanstd(val_adapted), 3)

    # save output file
    np.savetxt(f"{outfilename}.csv", [list(out_final.values())], header=','.join(out_final.keys()), delimiter=",",
               fmt='%.3f', comments="")
    print(f"{space_cons[3]}Saved results to {outfilename}.csv")


def plot_err(data, errdir, errfilename, space_cons, fig):
    x_data = data[0, :]
    y_data = data[1, :]

    fig.subplots_adjust(left=0.07, right=0.99, top=0.93, bottom=0.07, hspace=0.1, wspace=0.1)
    fig.suptitle("Analysis of AP parameters", fontweight='bold', fontsize=18)

    plt.plot(x_data, y_data)

    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)

    fig.savefig(f"{errdir}/{errfilename}.png", dpi=100)
    plt.clf()

    print(f"{space_cons[3]}Plot of raw data was saved to {errdir}/{errfilename}.png")


if __name__ == '__main__':
    args = parser()
    main(args)